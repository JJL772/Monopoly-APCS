import java.util.ArrayList;
import java.util.Random;

public class Piece implements IPiece
{
    public String Owner;
    public int Index = 0;
    public int Turns = 0;
    public int SkippedTurns = 0;
    public int ROwner = 0;
    public int Money = 0;
    public int num = 0;
    public boolean Moved = false;
    public boolean InJail = false;
    public ArrayList<String> Cards;

    Piece()
    {
        Owner = "Undefined";
        Cards = new ArrayList<String>();
    }

    @Override
    public String getName() {
        return Owner;
    }

    @Override
    public void setOwner(String owner) {
        Owner = owner;
    }

    public void setIndex(int i){
        Index = i;
    }

    public int getIndex()
    {
        return Index;
    }

    public void movePiece(int spaces)
    {
        if((Index + spaces) > 27)
        {
            int sp = Index;
            Index = (Index + spaces) - 27;
            Money += 200;
            if(num == 1) {
                Monopoly.Board[Index].setPiece1(this);
                Monopoly.Board[sp].setPiece1(null);
            }else {
                Monopoly.Board[Index].setPiece2(this);
                Monopoly.Board[sp].setPiece2(null);
            }
            this.payMortage();
        }
        else
        {
            int sp = Index;
            Index += spaces;
            if(num == 1) {
                Monopoly.Board[Index].setPiece1(this);
                Monopoly.Board[sp].setPiece1(null);
            }else {
                Monopoly.Board[Index].setPiece2(this);
                Monopoly.Board[sp].setPiece2(null);
            }
        }
        if(spaces > 0){
            Moved = true;
        }

    }

    public void addTurn()
    {
        Turns++;
    }

    public int getTurns()
    {
        return Turns;
    }

    public void addSkippedTurns(int skipped)
    {
        SkippedTurns = skipped;
    }

    public int getSkippedTurns(){
        return SkippedTurns;
    }

    public boolean canMove(){
        return SkippedTurns == 0;
    }

    public void setROwner(int owner){
        ROwner = owner;
    }

    public int getROwner(){
        return ROwner;
    }

    public Square getSquare(){
        return Monopoly.Board[Index];
    }

    public void handleCharge(){
        if (Monopoly.Board[Index].getPrice() > 0 && Moved){
            System.out.println("You've been charged " + Monopoly.Board[Index].getPrice() + " for staying at " + Monopoly.Board[Index].getName());
            Moved = false;
        }
    }

    public void pickChance(){
        String card = Chance.getChance();
        Cards.add(card);
        System.out.println("You drew a " + card);
        if(card.equals(Chance.Money1)){
            Random r = new Random(101204);
            int f = r.nextInt(300)+1;
            this.Money += f;
            Cards.remove(card);
            System.out.println("you got $" + f);
        }
    }

    public boolean hasChance(String card){
        return Cards.contains(card);
    }

    public void useChance(String card){
        if(Cards.contains(card)){
            Cards.remove(card);
        }
    }

    public void payMortage(){
        for(Square sq : Monopoly.Board){
            if(sq.getOwner() == num){
                removeMoney(sq.getPrice());
            }
        }
    }

    public void removeMoney(int ammt){
        if(Money - ammt < 0){
            Money =0;
        }
        else{
            Money -= ammt;
        }
    }
}
