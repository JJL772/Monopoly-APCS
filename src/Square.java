public class Square extends SquareType
{
    private boolean bCanBeOwned;
    private int price;
    public Piece Player1;
    public Piece Player2;

    Square(String name, String type, int price, boolean ownable)
    {
        this.name = name;
        this.tileType = type;
        this.price = price;
        this.bCanBeOwned = ownable;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public boolean IsOwnable()
    {
        return bCanBeOwned;
    }

    public void purchase(int owner){
        if(bCanBeOwned && this.owner == 0){
            if(owner ==1){
                if(Monopoly.Player1P.Money >= price) {
                    Monopoly.Player1P.Money -= price;
                    super.owner = owner;
                }else{
                    System.out.println("Player 1 does not have enough money for that!");
                }
            }else{
                if(Monopoly.Player2P.Money >= price) {
                    Monopoly.Player2P.Money -= price;
                    super.owner = owner;
                }else{
                    System.out.println("Player 2 does not have enough money for that!");
                }
            }
        }
        else{
            System.out.println("Unable to Purchase");
        }
    }

    public void sell(){
        if(owner == 1){
            Monopoly.Player1Money += price;
        }else if(owner == 2){
            Monopoly.Player1Money += price;
        }
        super.owner = 0;
    }

    public boolean isOwned(){
        return owner != 0;
    }

    public String getName1() {
        if(Player1 != null) {
            return Player1.getName();
        }else{
            return "   ";
        }
    }

    public String getName2(){
        if(Player2 != null){
            return Player2.getName();
        }else{
            return "   ";
        }
    }

    public void setPiece1(Piece piece){
        Player1 = piece;
    }

    public void setPiece2(Piece piece){
        Player2 = piece;
    }
}
