//Property
//Other
//Jail
//Chance
//Lux
public abstract class SquareType
{
    protected String name;
    protected String tileType;
    protected int owner;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTileType()
    {
        return tileType;
    }

    public void setTileType(String tileType)
    {
        this.tileType = tileType;
    }

    public int getOwner()
    {
        return owner;
    }

    public void setOwner(int owner)
    {
        this.owner = owner;
    }
}
