public class CBoard
{
    public static void PrintBoard()
    {
        System.out.println("------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------");
        System.out.println("|   "+Monopoly.Board[27].getName1()+"     | |   " +Monopoly.Board[0].getName1()+"     | |   "+Monopoly.Board[1].getName1()+"     | |   "+Monopoly.Board[2].getName1()+"     | |   "+Monopoly.Board[3].getName1()+"     | |   "+Monopoly.Board[4].getName1()+"     | |   "+Monopoly.Board[5].getName1()+"     | |   "+Monopoly.Board[6].getName1()+"     |");
        System.out.println("|   "+Monopoly.Board[27].getName2()+"     | |   "+Monopoly.Board[0].getName2()+"     | |   "+Monopoly.Board[1].getName2()+"     | |   "+Monopoly.Board[2].getName2()+"     | |   "+Monopoly.Board[3].getName2()+"     | |   "+Monopoly.Board[4].getName2()+"     | |   "+Monopoly.Board[5].getName2()+"     | |   "+Monopoly.Board[6].getName2()+"     |");
        System.out.println("|          | |   Mowry  | |  Yellow  | |          | | RAILROAD | |   China  | |  Unnamed | |          |");
        System.out.println("|    GO!   | |   Blvd.  | |   Road   | |  Chance  | |     1    | |  Store   | |   HOTEL  | | JAIL :(  |");
        System.out.println("|          | |   $100   | |   $150   | |          | |   $200   | |   $150   | |   $150   | |          |");
        System.out.println("------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------");
        System.out.println("------------                                                                               ------------");
        System.out.println("|   "+Monopoly.Board[26].getName1()+"     |                                                                               |    "+Monopoly.Board[7].getName1()+"    |");
        System.out.println("|   "+Monopoly.Board[26].getName2()+"     |                                                                               |    "+Monopoly.Board[7].getName2()+"    |");
        System.out.println("|  Disney- |                                                                               |  Grocery |");
        System.out.println("|   Land   |                                                                               |   Store  |");
        System.out.println("|   $450   |                                                                               |   $200   |");
        System.out.println("------------                                                                               ------------");
        System.out.println("------------                                                                               ------------");
        System.out.println("|   "+Monopoly.Board[25].getName1()+"     |                                                                               |    "+Monopoly.Board[8].getName1()+"    |");
        System.out.println("|   "+Monopoly.Board[25].getName2()+"     |                                                                               |    "+Monopoly.Board[8].getName2()+"    |");
        System.out.println("|  Disney  |                                                                               |   Fry's  |");
        System.out.println("|   World  |                                                                               |Electronic|");
        System.out.println("|   $600   |                                                                               |   $350   |");
        System.out.println("------------                                                                               ------------");
        System.out.println("------------                                                                               ------------");
        System.out.println("|   "+Monopoly.Board[24].getName1()+"     |                                                                               |    "+Monopoly.Board[9].getName1()+"    |");
        System.out.println("|   "+Monopoly.Board[24].getName2()+"     |                                                                               |    "+Monopoly.Board[9].getName2()+"    |");
        System.out.println("| RAILROAD |                                                                               | RAILROAD |");
        System.out.println("|    4     |                                                                               |     2    |");
        System.out.println("|  $150    |                                                                               |   $200   |");
        System.out.println("------------                                                                               ------------");
        System.out.println("------------                                                                               ------------");
        System.out.println("|   "+Monopoly.Board[23].getName1()+"     |                                                                               |    "+Monopoly.Board[10].getName1()+"    |");
        System.out.println("|   "+Monopoly.Board[23].getName2()+"     |                                                                               |    "+Monopoly.Board[10].getName2()+"    |");
        System.out.println("|          |                                                                               |          |");
        System.out.println("|  Chance  |                                                                               |  Chance  |");
        System.out.println("|          |                                                                               |          |");
        System.out.println("------------                                                                               ------------");
        System.out.println("------------                                                                               ------------");
        System.out.println("|   "+Monopoly.Board[22].getName1()+"     |                                                                               |    "+Monopoly.Board[11].getName1()+"    |");
        System.out.println("|   "+Monopoly.Board[22].getName2()+"     |                                                                               |    "+Monopoly.Board[11].getName2()+"    |");
        System.out.println("|  Random  |                                                                               |   Tech   |");
        System.out.println("|   Store  |                                                                               |   Store  |");
        System.out.println("|   $400   |                                                                               |   $250   |");
        System.out.println("------------                                                                               ------------");
        System.out.println("------------                                                                               ------------");
        System.out.println("|   "+Monopoly.Board[21].getName1()+"     |                                                                               |    "+Monopoly.Board[12].getName1()+"    |");
        System.out.println("|   "+Monopoly.Board[21].getName2()+"     |                                                                               |    "+Monopoly.Board[12].getName2()+"    |");
        System.out.println("|Shoe Store|                                                                               |   School |");
        System.out.println("|   $390   |                                                                               |   $300   |");
        System.out.println("------------                                                                               ------------");
        System.out.println("------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------");
        System.out.println("|   "+Monopoly.Board[20].getName1()+"     | |   "+Monopoly.Board[19].getName1()+"     | |   "+Monopoly.Board[18].getName1()+"     | |   "+Monopoly.Board[17].getName1()+"     | |   "+Monopoly.Board[16].getName1()+"     | |   "+Monopoly.Board[15].getName1()+"     | |   "+Monopoly.Board[14].getName1()+"     | |   "+Monopoly.Board[13].getName1()+"     |");
        System.out.println("|   "+Monopoly.Board[20].getName2()+"     | |   "+Monopoly.Board[19].getName2()+"     | |   "+Monopoly.Board[18].getName2()+"     | |   "+Monopoly.Board[17].getName2()+"     | |   "+Monopoly.Board[16].getName2()+"     | |   "+Monopoly.Board[15].getName2()+"     | |   "+Monopoly.Board[14].getName2()+"     | |   "+Monopoly.Board[13].getName2()+"     |");
        System.out.println("|    GO    | |   Luxury | |          | |          | | RAILROAD | |  Cinema  | |    The   | |   FREE   |");
        System.out.println("|    TO    | |    TAX   | |  utility | |  Chance  | |     3    | |          | |    Mall  | |  PARKING |");
        System.out.println("|   JAIL   | |   -$250  | |   -$200  | |          | |   $200   | |   $350   | |   $300   | |          |");
        System.out.println("------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------");

    }
}
