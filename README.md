This project was created for the AP Computer Science class at Washington High School for the 2017-2018 school year.
As per the rubric for the project, using features of the language that were not taught (enums, reflection, etc.) would result in a deduction of points.

This project served as a reference for students and was not turned in for credit.